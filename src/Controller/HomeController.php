<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package App\Controller
 * @Route("/")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param SessionInterface $session The current session
     *
     * @return Response
     */
    public function index(
        SessionInterface $session
    ) {
        if (!$session->isStarted()) {
            $session->start();
        }

        $session->set('test', 'test12345');

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/test", name="test")
     */
    public function test()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        return $this->render('home/index.html.twig', [
            'controller_name' => 'Test Role Controller',
        ]);
    }
}
