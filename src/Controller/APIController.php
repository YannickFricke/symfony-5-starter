<?php

namespace App\Controller;

use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class APIController
 * @package App\Controller
 * @Route("/api")
 */
class APIController extends AbstractController
{
    /**
     * @Route("/users", name="api_users", methods={"GET"})
     * @SWG\Response(
     *     response="200",
     *     description="When the users could be fetched",
     * )
     * @SWG\Parameter(
     *     name="limit",
     *     type="number",
     *     description="Amount of users to fetch",
     *     in="query",
     * )
     * @SWG\Tag(
     *     name="User",
     * )
     */
    public function users()
    {
        return $this->json([
            [
                'id' => 1,
                'username' => 'my test user',
                'email' => 'my@email.com',
            ]
        ]);
    }
}