## [1.0.4](https://gitlab.com/YannickFricke/symfony-5-starter/compare/v1.0.3...v1.0.4) (2020-03-06)


### Bug Fixes

* **publish:** hopefully fixed the publish script ([38aba02](https://gitlab.com/YannickFricke/symfony-5-starter/commit/38aba02d718c8e7e57023104260b9c8a1977ea99))
* **publish:** marked the verifyConditions script as executable ([175fc82](https://gitlab.com/YannickFricke/symfony-5-starter/commit/175fc820eecdcf089c06f409a32c9f989723c46a))

## [1.0.3](https://gitlab.com/YannickFricke/symfony-5-starter/compare/v1.0.2...v1.0.3) (2020-03-06)


### Bug Fixes

* **publish:** fixed the publish script ([07dbc8b](https://gitlab.com/YannickFricke/symfony-5-starter/commit/07dbc8b4069629b50ff0db916174cbd87622c07c))

## [1.0.2](https://gitlab.com/YannickFricke/symfony-5-starter/compare/v1.0.1...v1.0.2) (2020-03-06)


### Bug Fixes

* **publish:** fixed the publish script ([7059bca](https://gitlab.com/YannickFricke/symfony-5-starter/commit/7059bca2f4a33e1c91bd5fa44a32485e90e04657))

## [1.0.1](https://gitlab.com/YannickFricke/symfony-5-starter/compare/v1.0.0...v1.0.1) (2020-03-06)


### Bug Fixes

* **assets:** added missing css files ([c55335e](https://gitlab.com/YannickFricke/symfony-5-starter/commit/c55335e9de218f9a23cb36391b0301bddaf24859))
* **release:** fixed the release ([82a0d6a](https://gitlab.com/YannickFricke/symfony-5-starter/commit/82a0d6ac5462ec5e2990fcad63f0880466130cc2)), closes [#2](https://gitlab.com/YannickFricke/symfony-5-starter/issues/2)

# 1.0.0 (2020-03-06)


### Bug Fixes

* **e2e:** changed the homepage test ([441e47f](https://gitlab.com/YannickFricke/symfony-5-starter/commit/441e47f2f94602f8237ee10b2524768e84e4a323))
* **git:** fixed the husky hook ([4c2b52d](https://gitlab.com/YannickFricke/symfony-5-starter/commit/4c2b52daec05e2634529715a4baaa518d25e7a6e))


### Features

* **api:** added the Nelmio API doc bundle ([023ab06](https://gitlab.com/YannickFricke/symfony-5-starter/commit/023ab06dd7868da54604180c792452612f4f9d4b)), closes [#6](https://gitlab.com/YannickFricke/symfony-5-starter/issues/6)
* **auth:** added user authentication ([b9baa6d](https://gitlab.com/YannickFricke/symfony-5-starter/commit/b9baa6dcd048c430eb48661bf09d227e552d9539)), closes [#8](https://gitlab.com/YannickFricke/symfony-5-starter/issues/8)
* **charts:** added chart.js ([01cc536](https://gitlab.com/YannickFricke/symfony-5-starter/commit/01cc536e8b26fb71e30b1423cb8915a9d0d7fd56)), closes [#10](https://gitlab.com/YannickFricke/symfony-5-starter/issues/10)
* **docker:** added docker support ([67e10f7](https://gitlab.com/YannickFricke/symfony-5-starter/commit/67e10f72b6bc9125346b407183a0c26e48b33493)), closes [#7](https://gitlab.com/YannickFricke/symfony-5-starter/issues/7)
* **dx:** added reload on save ([bc26a9b](https://gitlab.com/YannickFricke/symfony-5-starter/commit/bc26a9b7bd557d8bde5919918f28068bc5798ad7)), closes [#11](https://gitlab.com/YannickFricke/symfony-5-starter/issues/11)
* **forms:** added a tailwindcss form theme ([aba76e0](https://gitlab.com/YannickFricke/symfony-5-starter/commit/aba76e0cc2163f30660ee1dcbf029350945959a4)), closes [#9](https://gitlab.com/YannickFricke/symfony-5-starter/issues/9)
* **git:** added husky for testing and linting the source files on commit ([2fd7391](https://gitlab.com/YannickFricke/symfony-5-starter/commit/2fd7391ce3ff7120b498600d31923ad83b4f0547)), closes [#3](https://gitlab.com/YannickFricke/symfony-5-starter/issues/3)
* **lint:** added eslint and typescript support ([8ba01ec](https://gitlab.com/YannickFricke/symfony-5-starter/commit/8ba01ec59b7d5fcd8af05be661bb8ea5f0ab0b3b))
* **lint:** added php-cs-fixer ([8a6c4ae](https://gitlab.com/YannickFricke/symfony-5-starter/commit/8a6c4aedb61977a3ebc70b8a6a653165904f37bc))
* **session:** added redis support ([20ac145](https://gitlab.com/YannickFricke/symfony-5-starter/commit/20ac145760eeda58abc20c1443e267bf02fa6dc2)), closes [#12](https://gitlab.com/YannickFricke/symfony-5-starter/issues/12)
* **tests:** added end2end tests ([73b9615](https://gitlab.com/YannickFricke/symfony-5-starter/commit/73b9615df85ebd11f996b38e9f2a3adfc4e31956)), closes [#5](https://gitlab.com/YannickFricke/symfony-5-starter/issues/5)
