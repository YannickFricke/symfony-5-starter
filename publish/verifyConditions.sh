#!/usr/bin/env bash

sed --help >/dev/null 2>&1

if [ $? -eq 127 ]; then
  echo "sed is not installed."
  exit 1
fi
