#!/usr/bin/env bash

if [ "$#" -ne 1 ]; then
  echo "The first argument must be the next version"
fi

NextVersion=$1

sed -i -e "s/version:\ .*/version:\ $NextVersion/" config/packages/nelmio_api_doc.yaml
