FROM php:7.4.2-fpm as symfonyStage

RUN apt-get update
RUN apt-get install -y wget

RUN wget https://get.symfony.com/cli/installer -O - | bash

RUN mv $HOME/.symfony/bin/symfony /usr/local/bin/symfony

FROM php:7.4.2-fpm as composerStage

WORKDIR /app

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"

FROM php:7.4.2-fpm

RUN apt-get update
RUN apt-get install -y git libzip-dev unzip

RUN docker-php-ext-install zip pdo pdo_mysql

COPY --from=symfonyStage /usr/local/bin/symfony /usr/local/bin/symfony
COPY --from=composerStage /app/composer.phar /usr/local/bin/composer

WORKDIR /app

COPY . .

RUN composer install
