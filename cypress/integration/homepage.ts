describe('Homepage', () => {
    it('should have the correct title', () => {
        cy.visit('http://localhost:8000/');

        cy.title().should(
            'equal',
            'Home — Symfony starter',
        );
    });

    it('should show a friendly message', () => {
        cy.get('.example-wrapper')
            .should(
                'contain.text',
                'This friendly message is coming from:',
            );
    });
});
